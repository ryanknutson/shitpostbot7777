--- get a list of all sources/templates with number of usages
SELECT source_id, COUNT(*) AS source_count FROM shitpost CROSS JOIN LATERAL unnest(source) AS source_id GROUP BY source_id ORDER BY source_count DESC;
SELECT template, COUNT(*) AS template_count FROM shitpost GROUP BY template ORDER BY template_count DESC;


--- same as above but for a single value

SELECT unnest(source) AS source_id, COUNT(*) AS source_count
FROM shitpost
WHERE ARRAY[source id] @> source
GROUP BY source_id;


SELECT template AS template_id, COUNT(*) AS template_count
FROM shitpost
WHERE template = 'template id'
GROUP BY template_id;

