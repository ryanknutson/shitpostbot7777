--- TODO add uploader to templates, source

CREATE TABLE template (
  id SERIAL PRIMARY KEY,
  filename TEXT,
  data bytea NOT NULL,
  crops json NOT NULL,
  uploaded TIMESTAMPTZ NOT NULL DEFAULT NOW(),
  isapproved BOOLEAN NOT NULL DEFAULT FALSE,
  data_hash CHAR(32) UNIQUE NOT NULL
);

CREATE TABLE source (
  id SERIAL PRIMARY KEY,
  filename TEXT,
  data bytea NOT NULL,
  uploaded TIMESTAMPTZ NOT NULL DEFAULT NOW(),
  isapproved BOOLEAN NOT NULL DEFAULT FALSE,
  data_hash CHAR(32) UNIQUE NOT NULL
);

CREATE TABLE shitpost (
  id SERIAL PRIMARY KEY,
  data bytea NOT NULL,
  uploaded TIMESTAMPTZ NOT NULL DEFAULT NOW(),
  template INT,
  source INT[]
);



--- get the combined name of template + shitpost(s)
CREATE OR REPLACE FUNCTION getCombinedName(shitpost_id INT)
RETURNS TEXT AS $$
DECLARE
  template_name TEXT;
  source_names TEXT[];
  full_name TEXT;
BEGIN
  -- Get the template name if available
  SELECT
    t.filename
  INTO
    template_name
  FROM
    template t
  WHERE
    t.id = (SELECT template FROM shitpost WHERE id = $1);

  -- Get the source names if available
  SELECT
    ARRAY(
      SELECT
        s.filename
      FROM
        source s
        JOIN unnest((SELECT source FROM shitpost WHERE id = $1)) WITH ORDINALITY t(src, ord) ON s.id = t.src
      WHERE
        s.filename IS NOT NULL
      ORDER BY
        t.ord
    )
  INTO
    source_names;

  -- Concatenate the parts to get the full name
	-- use this if you want ' + ' separated
  /* full_name := COALESCE(template_name, '') || ' + ' || COALESCE(ARRAY_TO_STRING(source_names, ' + '), ''); */
	-- use this if you want a space seperated
	full_name := COALESCE(template_name, '') || ' ' || COALESCE(ARRAY_TO_STRING(source_names, ' '), '');

  RETURN full_name;
END;
$$ LANGUAGE plpgsql;

--- list shitposts and the template + sources that make it up
CREATE VIEW shitpost_view AS
SELECT
    sp.id,
    sp.uploaded,
    getCombinedName(sp.id) AS filename,
    sp.data,
    sp.template AS template_id,
    (
      SELECT t.filename
      FROM template t
      WHERE t.id = sp.template
    ) AS template_filename,
    jsonb_agg(
        jsonb_build_object(
            'id', s.id,
            'filename', s.filename
        ) ORDER BY s.id DESC
    ) AS sources,
    ARRAY_AGG(s.id ORDER BY s.id DESC) AS source_ids,
    COUNT(s.id) AS source_count
FROM
    shitpost sp
LEFT JOIN
    source s ON s.id = ANY(sp.source)
GROUP BY
    sp.id, sp.uploaded, sp.template
ORDER BY
    sp.uploaded DESC;

--- list sources and the shitposts they were used in
CREATE VIEW source_view AS
SELECT
    source.id,
    source.filename,
    source.uploaded,
    CASE
      WHEN COUNT(sp.id) > 0 THEN
        jsonb_agg(
          jsonb_build_object('id', sp.id, 'filename', getCombinedName(sp.id))
          ORDER BY sp.id DESC
        )
      ELSE
        NULL
    END AS shitposts,
    CASE
      WHEN COUNT(sp.id) > 0 THEN
        ARRAY(
            SELECT
                sp.id
            FROM
                shitpost sp
            WHERE
                source.id = ANY(sp.source)
            ORDER BY
                sp.id DESC
        )
      ELSE
        NULL
    END AS shitpost_ids,
    source.data
FROM
    source
LEFT JOIN
    shitpost sp ON source.id = ANY(sp.source)
GROUP BY
    source.id, source.filename
ORDER BY
    source.id DESC
;

--- list templates and the shitposts they were used in
CREATE VIEW template_view AS
SELECT
    template.id,
    template.filename,
    template.uploaded,
    template.crops,
    CASE
      WHEN COUNT(sp.id) > 0 THEN
        jsonb_agg(
          jsonb_build_object('id', sp.id, 'filename', getCombinedName(sp.id))
          ORDER BY sp.id DESC
        )
      ELSE
        NULL
    END AS shitposts,
    CASE
      WHEN COUNT(sp.id) > 0 THEN
        ARRAY(
            SELECT
                sp.id
            FROM
                shitpost sp
            WHERE
                template.id = sp.template
            ORDER BY
                sp.id DESC
        )
      ELSE
        NULL
    END AS shitpost_ids,
    template.data
FROM
    template
LEFT JOIN
    shitpost sp ON template.id = sp.template
GROUP BY
    template.id, template.filename
ORDER BY
    template.id DESC
;


GRANT SELECT, INSERT, UPDATE, DELETE ON ALL TABLES IN SCHEMA public TO spb7777;
GRANT USAGE, SELECT ON ALL SEQUENCES IN SCHEMA public TO spb7777;
GRANT EXECUTE ON ALL FUNCTIONS IN SCHEMA public TO spb7777;
