const express = require('express')
const app = express()
const port = 3555

const sharp = require('sharp')
const path = require('node:path')

const fileUpload = require('express-fileupload')
app.use(fileUpload())
app.set('view engine', 'pug')
app.use(express.static('public'))


// sql functions
const sqlFunc = require('./src/js/sqlFunc')

// shitpost function
const shitpost = require('./src/js/shitpost')

// grab a buffer of a template with crops outlined
const viewTemplate= require('./src/js/viewTemplate')

// validate that requested type is valid
const validType = require('./src/js/validType')


// shitpost endpoint
app.get('/shitpost', async (req, res) => {
  try {
    const retID = (await shitpost()).id

    res.status(200).redirect(`/view/shitpost/${retID}`)
  } catch (e) {
    res.status(400).send(e.message)
  }
})

app.get('/', async (req, res) => {
  try {
    const posts = await sqlFunc.getHomepagePosts()
    res.render('home', { posts })
  } catch (e) {
    res.status(400).send(e.message)
  }
})


// create source or template
app.get('/upload/:type', (req, res) => {
  switch(req.params.type) {
  case 'source':
    res.render('sourceUpload.pug')
    break;
  case 'template':
    res.render('templateUpload.pug')
    break;
  default:
    res.status(404).send('Not Found')
  }
})

// file upload POST
app.post('/upload/:type', async (req, res) => {
  console.log(req.params.type)
  try {
    // Log the file to the console
    console.log(req.files);

    // get the file name set by user
    const filename = req.body.name
    console.log('filename', filename)

    let retID, imgData

    switch (req.params.type) {
    case 'template':

      // TODO check that x and y data are realistic
      // filter out crops with zero width or height
      const checkWH = crop => (crop.w > 0) && (crop.h > 0)
      const crops = JSON.parse(req.body.crops).filter(checkWH)

      // if there are no crops, throw an error
      if (crops.length == 0) throw new Error('no-crops')

      // upload to database, return the id of uploaded source
      imgData = await sharp(req.files.image.data).png().toBuffer()
      // retID = await sqlFunc.uploadTemplate(name, img, crops)
      retID = await sqlFunc.upload({
        type: 'template',
        filename, imgData, crops
      })
      break
    case 'source':
      imgData = await sharp(req.files.image.data).png().toBuffer()
      // retID = await sqlFunc.uploadSource(name, img)
      retID = await sqlFunc.upload({
        type: 'source',
        filename, imgData
      })
      break
    default:
      res.status(404).send('Not Found')
    }
    
    // All good
    res.location(`/view/${req.params.type}/${retID}`)
    res.status(200)
    res.end()
  } catch(err) {
    // TODO error handling
    // TODO handle multiple files (should never happen)
    // TODO handle no image
    console.error(err)
    res.status(409).send(err.message)
    // res.status(500).send( { error: err.message })
    console.log('Error!', err.message)
    // res.status(586).json({ error: 'Something failed!' })
  }
})




app.get('/gallery/:type/:page?', async (req, res) => {
  try {
    let { type, page } = req.params

    // set page to 1 if no page entered
    // also cast to string
    page = page??+1

    // check if type is valid
    if (!validType(type)) throw new Error('invalid-type')

    // check that requested page is valid
    const numPages = +(await sqlFunc.getLastPage({ type }))

    if ( (page < 1) || (page > numPages) ) throw new Error('invalid-page')

    // const posts = await sqlFunc.getLatest({ type, page })
    const posts = await sqlFunc.grabImages({ type, page })

    console.log(posts)

    res.render('gallery', {
      posts, type, page, numPages
    })

  } catch (e) {
    res.send(e.message).status(400)
  }

})


// page to view a single template, source, or shitpost
// TODO error handle if doesn't exist
app.get('/view/:type/:id', async (req, res) => {
  try {
    const { id, type } = req.params

    // check that type is valid
    if ( !(validType(type) || (type == 'templateCrops')) )
      throw new Error('invalid-type')

    // check that requested image is valid
    const imgExists = await sqlFunc.imgExists({ type, id })
    if (!imgExists) throw new Error('invalid-image')

    // get the metadata for image
    // const metadata = await sqlFunc.metadata({ type, id })
    const sqlRes = await sqlFunc.grabImages({ type, id })

    console.log(sqlRes[0])

    // render the page and pass the source image url
    res.render('viewimg', {
      imgPath: `/img/${type}/${id}.png`,
      type,
      metadata: sqlRes[0]
    })
  } catch (e) {
    res.send(e.message).status(400)
  }
})

app.use('/btstrp', express.static(path.join(__dirname, 'node_modules/bootstrap/dist')))
app.use('/popper', express.static(path.join(__dirname, 'node_modules/@popperjs/core/dist/cjs/')))


// api to get a single template, source, shitpost,
// or template with crops outlined
// returns a buffer
app.get('/img/:type/:id.png', async (req, res) => {
  try {
    const { id, type } = req.params
    // if (!validType(type)) throw new Error('invalid-type')

    console.log(type)
    const img = (await sqlFunc.grabImages({type, id, data: true}))[0]

    // check that requested image exists
    // TODO move to try/catch
    if (!img) throw new Error('invalid-image')

    console.log(img)

    // if successful, send to client
    res.send(img.data)
    return
  } catch (e) {
    console.error(e)
    res.send(400)
  }
})

app.listen(port, () => {
  console.log(`shitpostbot7777 listening on port ${port}`)
})



// discord.js stuff
const fs = require('node:fs')
const { Client, IntentsBitField, EmbedBuilder, AttachmentBuilder } = require('discord.js');

const clientId = process.env.DISCORD_CLIENTID
const guildId = process.env.DISCORD_GUILDID

// const client = new Discord.Client()
const client = new Client({
  intents: [
    IntentsBitField.Flags.Guilds,
    // IntentsBitField.Flags.GuildMembers,
    IntentsBitField.Flags.GuildMessages,
    IntentsBitField.Flags.MessageContent,
  ]
});


// !shitpost command
const discordShitpost = async (msg) => {
  try {
    // reply to user to let them know command worked
    msg.reply('shitpost time 😈')

    // get the return id and image buffer from shitpost
    const ret = await shitpost()

    console.log('ret')
    console.log(ret)

    // attach the file and create a message
    // const file = new AttachmentBuilder(ret.buf)
    const embed = new EmbedBuilder()
    .setColor(0x7fffd4)
	  .setTitle(`Shitpost #${ret.id}`)
	  .setURL(`https://shitpostbot.gamergirlpee.xyz/view/shitpost/${ret.id}`)
	  .setImage(`https://shitpostbot.gamergirlpee.xyz/img/shitpost/${ret.id}.png`)
    // .setImage(`attachment://shitpost-${ret.id}.png`)

    // send message with file
    // msg.channel.send({ embeds: [embed], files: [file] })
    msg.channel.send({ embeds: [embed] })
  } catch (err) {
    msg.channel.send('Error! ' + err.message)


  }
}

const discordGrab = async (msg) => {
  try {
    const cmds = msg.content.split(" ")
    const type = cmds[1]

    // validate that commands are formatted !spbgrab TYPE ID
    if (cmds.length != 3) throw new Error('invalid format!')

    // validate type
    if (!validType(type)) throw new Error('invalid type requested!')
    
    let id 
    if (cmds[2] == 'rand' || cmds[2] == 'random') {
      // if random, set to null
      id = null
    } else {
      // if not random, check that it's a valid number
      id = Number(cmds[2])
      if (isNaN(id)) throw new Error('id is not a number!')
    }

    // get the file w/ info
    const ret = (await sqlFunc.grabImages({type, id}))[0]
    console.log(ret)

    // type with first letter capitalized
    const typeFmt = type.charAt(0).toUpperCase() + type.slice(1)

    // create message and embed
    // const file = new AttachmentBuilder(ret.data)
    const embed = new EmbedBuilder()
    .setColor(0x7fffd4)
	  .setTitle(`${typeFmt} #${ret.id}`)
    .setDescription(ret.filename??`a fine ${type}`)
	  .setURL(`https://shitpostbot.gamergirlpee.xyz/view/${type}/${ret.id}`)
	  .setImage(`https://shitpostbot.gamergirlpee.xyz/img/${type}/${ret.id}.png`)
    // .setImage(`attachment://${type}-${ret.id}.png`)

    // send message with file
    // msg.channel.send({ embeds: [embed], files: [file] })
    msg.channel.send({ embeds: [embed] })
  } catch (err) {
    // error handling
    msg.reply(err.message + '\nsyntax: `!spbgrab [type] [id]`')
    return
  }
}


const axios = require('axios')


const discordUpload = async (msg, attachment) => {

  
  // if no filename set to null
  // TODO maybe prompt for filename?
  const filename = msg.content.replace(/!spbupload/g, '').trim() || null
  // msg.reply('this function is not yet implimented!' + filename)

  const imgData = (await axios(attachment.url, { responseType: 'arraybuffer' })).data
  // console.log(image)
  // const buffer64 = Buffer.from(image.data, 'binary').toString('base64')
  // console.log(buffer64)
  

  await sqlFunc.upload({
    type: 'source',
    filename,
    imgData
  }).then((e) => {
    // msg.reply('Image uploaded!')

    const successEmbed = new EmbedBuilder()
    .setColor(0x7fffd4)
	  .setTitle(`source #${e}`)
    .setDescription(filename??`a fine source`)
	  .setURL(`https://shitpostbot.gamergirlpee.xyz/view/source/${e}`)
	  .setImage(`https://shitpostbot.gamergirlpee.xyz/img/source/${e}.png`)

    // msg.channel.send({ embeds: [embed] })
    msg.reply({ embeds: [successEmbed] })

  }).catch((err) => {
    msg.reply('❌ Error! ' + err.message + '⚠️')
  })

  // let ret = await sharp(imgData).flip().toBuffer()

  // const file = new AttachmentBuilder(ret)
  // const embed = new EmbedBuilder()
  // .setColor(0x7fffd4)
	// .setTitle(filename??'untitled')
  // .setImage(`attachment://${filename?(filename.replace(/[^\x00-\x7F]/g, "")).replace(/\s+/g, '-'):'untitled'}.png`)

  // // send message with file
  // msg.channel.send({ embeds: [embed], files: [file] })

}

const discordUploadHandler = async msg => {
  // TODO error handling for no image or non picture
  console.log(msg.attachments)

  msg.attachments.forEach((attachment, key) => {
    const attachmentURL = attachment.url
    console.log(`URL for key ${key}: ${attachmentURL}`)
    discordUpload(msg, attachment)
    // msg.channel.send(attachment.url)
  })
}

client.on('messageCreate', async (msg, type) => {
  try {
    // return if author is bot
    if (msg.author.bot) return;

    console.log(msg.content)

    let text = msg.content
    if (text.startsWith("!shitpost")) {
      discordShitpost(msg)
    } else if (text.startsWith("!spbgrab")) {
      discordGrab(msg)
    } else if (text.startsWith("!spbupload")) {
      await discordUploadHandler(msg)


    }
  } catch (err) {
    msg.channel.send('Error! ' + err.message)
    return
  }
})


client.on('ready', c => {
	console.log(`Discord bot ready! Logged in as ${c.user.tag}`);
});

client.login(process.env.DISCORD_TOKEN);
