// look for changes in image
imageInput.addEventListener('change', (e) => {
  const file = e.target.files[0];

  document.getElementById('imageName').value = file.name.replace(/\..+$/, '')
  // document.getElementById('imageName').placeholder = file.name.replace(/\..+$/, '')

});

const uploadSource = document.getElementById('uploadSource');
uploadSource.addEventListener('click', () => {

  const imageInput = document.getElementById('imageInput');
  const file = imageInput.files[0];
  console.log(file)

  if (file) {
    // Create a FormData object to send the data
    const formData = new FormData();

    // attach the image
    formData.append('image', file);

    // add the user's chosen filename to form data
    const name = document.getElementById('imageName').value
    formData.append('name', name)

    // upload image and crop data
    fetch('/upload/source', {
        method: 'POST',
        body: formData
    })
    .then(res => {
      console.log(res)
      if (res.ok) {
        // set modal image
        const imgID = /\d+/.exec(res.headers.get('Location'))[0]
        document.getElementById('successImg').src = `/img/source/${imgID}.png`

        // set the success button to go to image
        document.getElementById('successButton').href = res.headers.get('Location')

        // show modal on success
        const successModal = new bootstrap.Modal(document.getElementById('successModal'))
        successModal.show()
      } else {
        console.error(res.statusText)
        const failureModal = new bootstrap.Modal(document.getElementById('failureModal'))
        failureModal.show()
        document.getElementById('failureStatus').innerHTML = `Upload failure! (${res.status}): ${res.statusText}`
      }
      // Handle the server's response here
    })
  }
});
