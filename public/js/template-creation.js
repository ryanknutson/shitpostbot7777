const container = document.getElementById('imgPreviewContainer');
const imageInput = document.getElementById('imageInput');
let jcropStage = null; // Store Jcrop stage instance

// Function to load Jcrop after the image is loaded
function loadJcrop(imageUrl) {
  const myImage = new Image();
  myImage.src = imageUrl;
  container.innerHTML = ''; // Clear previous image if any.
  container.appendChild(myImage);

  myImage.onload = function() {
    // Now that the image is loaded, we can attach Jcrop to it.
    if (jcropStage) {
      // Destroy the previous Jcrop stage instance if it exists
      jcropStage.destroy();
    }

    const options = {
      multi: true,
    };

    jcropStage = Jcrop.attach(myImage, options);

    // get coordinates of selected areas
    // TODO remove after testing
    jcropStage.listen('crop.change', (e) => {
      console.log('new coords:');
      for (const x of jcropStage.crops) {
        console.log(x.pos);
      }
    });
  };
}

// Listen for changes in the input field
imageInput.addEventListener('change', (e) => {
  const file = e.target.files[0];

  document.getElementById('imageName').value = file.name.replace(/\..+$/, '')
  // document.getElementById('imageName').placeholder = file.name.replace(/\..+$/, '')

  if (file) {
    // Create a URL for the selected image
    const imageUrl = URL.createObjectURL(file);
    loadJcrop(imageUrl); // Load Jcrop after the image is selected
  }
});

// Handle the "Crop and Upload" button click
const cropAndUploadButton = document.getElementById('cropAndUpload');
cropAndUploadButton.addEventListener('click', () => {
  // Use the jcropStage.crops array to get the coordinates.
  console.log('Upload button clicked');

  // print out coords
  console.log('Coordinates:');
  for (const x of jcropStage.crops) {
    console.log(x.pos);
  }

  const imageInput = document.getElementById('imageInput');
  const file = imageInput.files[0];
  console.log(file)

  if (file) {
    // Create a FormData object to send the data
    const formData = new FormData();

    // attach the image
    formData.append('image', file);

    // add the user's chosen filename to form data
    const name = document.getElementById('imageName').value
    formData.append('name', name)

    // add crop data
    let crops = []
    for (const sel of jcropStage.crops) {
      // console.log(sel.pos)
      crops.push(sel.pos)
    }
    formData.append('crops', JSON.stringify(crops))


    // upload image and crop data
    fetch('/upload/template', {
        method: 'POST',
        body: formData
      })
      .then(res => {
        console.log(res)
        if (res.ok) {
          // set modal image
          const imgID = /\d+/.exec(res.headers.get('Location'))[0]
          // document.getElementById('successImg').src = `/img/templateCrops/${imgID}.png`
          document.getElementById('successImg').src = `/img/template/${imgID}.png`

          // set the success button to go to image
          document.getElementById('successButton').href = res.headers.get('Location')
          
          // show modal on success
          const successModal = new bootstrap.Modal(document.getElementById('successModal'))
          successModal.show()
        }

        // Handle the server's response here
      })
      .catch(err => {
        console.error('Error:', err);
        // TODO show error modal
      })
  }
});
