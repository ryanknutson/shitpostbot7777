// return a buffer of a template with crops outlined
const sharp = require('sharp')
const sqlFunc = require('./sqlFunc')

// TODO error handling
const viewTemplate = async (id) => {

  // grab image from db
  const img = await sqlFunc.grabImg({
    type: 'template', id
  })


  // get width and height of image
  // TODO move this info to db
  await sharp(img.data)
  .metadata()
  .then(metadata => {
    img.width = metadata.width;
    img.height = metadata.height;
  })


  // set svg viewbox dimensions
  let overlaySvg = `<svg viewBox="0 0 ${img.width} ${img.height}">`

  // add crops to svg
  for (i of img.crops) {
    overlaySvg += `
    <rect x="${i.x}" y="${i.y}" width="${i.w}" height="${i.h}"
      stroke="red" stroke-opacity="0.8" stroke-width=".5%"
      fill-opacity="0" 
    />`
  }
  overlaySvg += '</svg>'

  const retImg = await sharp(img.data)
    .composite([{ input: Buffer.from(overlaySvg)}])
    .png()
    .toBuffer()

  return retImg
}

module.exports = viewTemplate
