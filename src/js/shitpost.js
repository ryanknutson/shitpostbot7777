const sharp = require('sharp')
const fs = require('node:fs').promises

const sqlFunc = require('./sqlFunc')

// get a source image
const grabSourceImg = async (width, height) => {
  // grab a buffer of a random source image
  // const res = await sqlFunc.grabSource()
  // const res = await sqlFunc.grabImg('source')
  const res = await sqlFunc.grabImage({
    type: 'source'
  })
  
  // return a resized buffer and the ID
  const buf = await sharp(res.data)
  .resize({ width, height, fit: 'fill' })
  .toBuffer()
  return { buf, id: res.id, name: res.filename }
}

// get a template image
// TODO delete this
const grabTemplateImg = async (id = null) =>
  await sqlFunc.grabImage({
    type: 'template',
    id
  })

// shitpost time
// can choose a template id, if not will be null
const shitpost = async (templateID = undefined, returnBuf = false) => {
  // store all ids for use in uploading to db
  let ids = {
    template: templateID,
    source: [],
    shitpost: undefined
  }

  console.log('starting shitpost 😈')

  // let template = await grabTemplateImg(templateID)
  let template = (await sqlFunc.grabImages({
    type: 'template',
    id: templateID,
    data: true
  }))[0]
  console.log('template data')
  console.log(template)

  // handle if there are no template images
  if (template == undefined) throw new Error('no-template-images')

  // set template id if not random
  // TODO cleanup
  templateID = template.id
  ids.template = template.id

  const crops = template.crops

  // create an array to hold sources, grab random images from db
  const sources = await sqlFunc.grabImages({
    type: 'shitpostSources',
    limit: crops.length
  })

  // handle if there are no sources
  if (sources.length == 0) throw new Error('no-source-images')

  // add sources to ids object, in an array
  for (i in sources) { ids.source.push(sources[i].id) }

  // insert crops to each source image
  for (i in crops) { sources[i].crops = crops[i] }


  // function to resize image, returns buffer
  const resizeImage = async (source) =>
    await sharp(source.data)
    .resize({
      width: source.crops.w,
      height: source.crops.h,
      fit: 'fill'
    })
    .toBuffer()


  // resize images
  await Promise.all(sources.map(async (source) => {
    source.resizedData = await resizeImage(source)
    delete source.data
  }));

  // console.log(sources)

  // get an array of image + positioning commands
  let cmds = []
  for (i of sources) {
    cmds.push({
      input: i.resizedData,
      top: i.crops.y,
      left: i.crops.x
    })
  }

  // console.log(cmds)

  // where the magic happens
  // composite the resized images onto the base image
  const imgData = await sharp(template.data)
    .composite(cmds).png().toBuffer()

  // const retID = await sqlFunc.uploadShitpost(output)
  const retID = await sqlFunc.upload({
    type: 'shitpost',
    imgData, 
    ids
  })
  // ids.shitpost = retID

  console.log('finished shitpost 😈')
  console.log(retID)

  // getcombinedname

  if (returnBuf) return { id: retID, buf: imgData }
  return retID
}


module.exports = shitpost
