// sql functions
// const { Pool, Client } = require('pg')

require('dotenv').config()

// validate that requested type is valid
const validType = require('./validType.js')

const postsPerPage = 12
// const dateFmt = 'YYYY-MM-DD HH12:MI PM TZ'
const dateFmt = 'YYYY-MM-DD HH24:MI'

const { Pool } = require('pg')
const pool = new Pool({
  user: process.env.PGUSER,
  host: process.env.PGHOST,
  database: process.env.PGDATABASE,
  password: process.env.PGPASSWORD,
  port: process.env.PGPORT,
  max: 20,
  idleTimeoutMillis: 30000,
  connectionTimeoutMillis: 2000,
})

// test sql connection
const testConn = async () => {
  const res = await pool.query('SELECT NOW()')

  console.log('sql connection test:', res.rows[0])
  return res
}

// upload source, template, and shitpost
// TODO try/catch
const upload = async opts => {
  try {
    if (!validType(opts.type)) throw new Error('invalid-type')

    console.log(`starting ${opts.type} upload to db`)

    let text, values
    switch (opts.type) {
      case 'source':
        text = `INSERT INTO source(filename, data, data_hash)
        VALUES($1, $2, MD5($2::bytea))
        RETURNING id`
        values = [opts.filename, opts.imgData]
        break;
      case 'template':
        text = `INSERT INTO template(filename, data, crops, data_hash)
        VALUES($1, $2, $3, MD5($2::bytea))
        RETURNING id`
        values = [opts.filename, opts.imgData, JSON.stringify(opts.crops)]
        break;
      case 'shitpost':
        text = `INSERT INTO shitpost(data, template, source)
        VALUES($1, $2, $3)
        RETURNING id, getcombinedname(id) AS filename`
        values = [
          opts.imgData, opts.ids.template, opts.ids.source
        ]
        break;
    }
    
    // returns the inserted id
    const res = await pool.query(text, values)
    console.log(res.rows[0])

    if (opts.type == 'shitpost') return res.rows[0]
    return res.rows[0].id
  } catch (e) {
    console.error(e)
    throw new Error('upload-failed')
  }
}

// check that image exists
const imgExists = async opts => {
  if (!validType(opts.type)) throw new Error('invalid-type')
  const text = `SELECT EXISTS(SELECT 1 FROM ${opts.type} WHERE id = $1);`
  const res = await pool.query(text, [opts.id])
  return res.rows[0].exists
}

const grabImages = async opts => {
  try {
    if (opts.type != 'shitpostSources') {
      if (!validType(opts.type)) throw new Error('invalid-type')
    }

    let text = `SELECT id, uploaded, filename,
    ${opts.data?'data,':''}
    ${(opts.type=='template')?'crops,':''}
    to_char(uploaded, '${dateFmt}') AS uploadedfmt,
    ${(opts.type=='shitpost')?'template_id, template_filename, sources, source_count':'shitposts'}
    FROM ${opts.type}_view
    `
    const values = []

    if (opts.id) {
      // get single by id
      text += 'WHERE id = $1'
      values.push(opts.id)
    } else if (opts.page) {
      // pagination
      text += `OFFSET $1 LIMIT $2`
      const offset = (opts.page - 1) * postsPerPage
      values.push(offset, postsPerPage)
    } else if (opts.type == 'shitpostSources') {
      text = `SELECT id, data FROM source ORDER BY random() LIMIT $1`
      values.push(opts.limit)
    } else {
      // get random by id
      text += 'ORDER BY random() LIMIT 1'
    }

    const res = await pool.query(text, values)
    return res.rows
  } catch (e) {
    console.log('error in grabImages')
    console.error(e)
  }
}

// get the total number of pages
const getLastPage = async opts => {
  if (!validType(opts.type)) throw new Error('invalid-type')

  // $1 is posts per page
  const text = `SELECT CEIL(COUNT(*) / $1::numeric)
  AS total_pages
  FROM ${opts.type}_view`

  return (await pool.query(text, [postsPerPage])).rows[0].total_pages
}

// get homepage latest posts
const getHomepagePosts = async numPosts => {
  const text = (['template', 'source', 'shitpost'].map(
  table => `(
    SELECT '${table}' as table_label, id, uploaded, filename,
    to_char(uploaded, '${dateFmt}') AS uploadedFmt
    FROM ${table}_view
    ORDER BY uploaded DESC
    LIMIT ${numPosts??6}
  )`.replace(/\r?\n|\r/g, ''))).join(' UNION ALL ')
  + ' ORDER BY uploaded DESC;'

  const res = await pool.query(text)

  // sort into object and return
  return res.rows.reduce((acc, row) => {
    const { table_label, ...rest } = row
    
    if (!acc[table_label]) acc[table_label] = []

    acc[table_label].push(rest)

    return acc
  }, {})
}

module.exports = {
  testConn,
  upload,
  grabImages,
  imgExists,
  getLastPage,
  getHomepagePosts
}
