const fs = require('node:fs')
const path = require('node:path')
const { Client, IntentsBitField, EmbedBuilder } = require('discord.js');
require('dotenv').config()

const clientId = process.env.DISCORD_CLIENTID
const guildId = process.env.DISCORD_GUILDID

// const client = new Discord.Client()
const client = new Client({
  intents: [
    IntentsBitField.Flags.Guilds,
    // IntentsBitField.Flags.GuildMembers,
    IntentsBitField.Flags.GuildMessages,
    IntentsBitField.Flags.MessageContent,
  ]
});

const testEmbed = new EmbedBuilder()
  .setColor(0x0099FF)
	.setTitle('ambatakum')
	.setURL('https://aispon.ge')
	// .setAuthor({ name: 'Some name', iconURL: 'https://i.imgur.com/AfFp7pu.png', url: 'https://discord.js.org' })
	// .setDescription('')
	// .setThumbnail('https://i.imgur.com/AfFp7pu.png')
	// .addFields(
	// 	{ name: 'Regular field title', value: 'Some value here' },
	// 	{ name: '\u200B', value: '\u200B' },
	// 	{ name: 'Inline field title', value: 'Some value here', inline: true },
	// 	{ name: 'Inline field title', value: 'Some value here', inline: true },
	// )
	// .addFields({ name: 'Inline field title', value: 'Some value here', inline: true })
	.setImage('https://upload.wikimedia.org/wikipedia/commons/7/70/Example.png')
	// .setTimestamp()
	// .setFooter({ text: 'Some footer text here', iconURL: 'https://i.imgur.com/AfFp7pu.png' });
//
//
let asyncTest = async (msg) => {
  console.log("Starting the function...");
  
  // Use setTimeout to wait for 3 seconds (3000 milliseconds)
  await new Promise(resolve => setTimeout(resolve, 3000));

  msg.channel.send('waited 3 seconds')
  
  console.log("Finished waiting for 3 seconds!");
}

client.on('messageCreate', async (msg) => {
  if (msg.author.bot) return;
  console.log(msg)
  console.log(msg.content)
  switch(msg.content) {
    case '!shitpost':
      msg.reply('shitpost time 😈')
      msg.channel.send({ embeds: [testEmbed] })
      if (msg.attachments) {
        msg.reply('has attachment!')
        console.log('has attachment!')
        console.log(msg.attachments)
      }
      await asyncTest(msg)
      break

  }
})


client.on('ready', c => {
	console.log(`Ready! Logged in as ${c.user.tag}`);
});

client.login(process.env.DISCORD_TOKEN);
